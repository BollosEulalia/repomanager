# RepoManager

A CLI-Tool used to create and manage repositories for students. 
Used at TU Graz for the courses "Introduction to structural Programming" and "Object oriented programming 1" to create repositories for participants of the course.


## Description

**This is only tested on linux**

* Create repositories
	- With teams up to 20 persons
* Create only a csv containing a repository list
* Download files
	- From a specific branch
	- From a specific (or multiple) directory (tree)
	- With a deadline
	- Create a simple download log
* Import
	- This is currently buggy and shouldn't be used
	- If new students participate and you need additional repositories
		+ To Avoid naming conflicts
* A DryRun feature to test your settings
	
For a more detailed description check [Usage](##-usage)

### Why was this created?

We needed something simple that can create repositories for the participants of our courses. 

A CSV is used as "database" because it is easily readable and editable by humans.

Also if someone doesn't want to use this tool for downloading it's easy to extract the ssh or http(s) column to just clone the repositories.


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
If you want to compile yourself clone this repository and run the make target for your OS.

## Usage

### Prerequisites

#### 1. You need a CSV file that contains at least the following information:

**If your university is using teachcenter you can create the initial list with the [tcwebcrawler](https://gitlab.com/BollosEulalia/tcwebcrawler)**

This repository will be referred to as *repolist repository* or *database repository*.

	* **Name** for each participant
	* **MatriculationNr**
		- If you don't have this you can also use any value you like as long as they are unique for everyone
	
	Optional values are:
	* Groups
	* Email
	
**Example:**
	
| Name         | MatricultionNr |
|:--------------:|:-----------:|
| Student1 |739 |
| Student2     |6 |
| Student3 | hello |
	
#### 2. Create a Repository on your gitlab server

* For a template you can use the [Template Repository](https://gitlab.com/BollosEulalia/repomanager-template)
	- The configuration files are explained there
* Create an access token for this repository
	- scopes: api
	- You can also use a private token but this is not recommended.
	- Add this token to *configrepo.toml*	


#### 3. Create a group for the repositories of the participants

* Create an access token for this group
	- scopes: api
	- You can also use a private token but this is not recommended.
	- Add this token to *fullconfig.toml*
	

**For all following options the repolist.csv in your config/database repository will be updated automatically**


### Create Repositories

First run  `./repomanager --createrepos -a <assignmentname>  --dryrun` .

If the output looks good then run without the `--dryrun` flag.

`./repomanager --createrepos -a <assignmentname>`

You can create repositories for multiple assignments at once

`./repomanager ---createrepos -a <assignmentname1> <assignmentname2>`

**If you create too many repositories and get timeouts (and the default is too short)**

`./repomanager --createrpos -a <assignmentname> -w <seconds>`


### Update Database

Run `./repomanager --update -a <assignmentname>`

### Download

NOTE: Deadline provided in examples is in UTC+0

**If you don't specify a submission branch run:**

It will default to the branch with the latest commit until deadline.

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z`

**With a specific submission branch**

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z -b submission `

This will also download all other branches. If you want **only** the **specified branch** then use 

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z -b submission --ignore`

**Specify the directory/tree of the repository**

`./repomanager --download --tree directory1 directory2`

**To speed up the downloads e.g. if you only need to download a specific group or repositories containing a specific substring**

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z -g <group>`

This will only download files for students where the *Groups* column in the csv file contains this substring.

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z --search team`

You can specifiy multiple searches and groups:

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z --search team1 team2 -g group1 group1`

This will only download files for students where the *SShRepo+Assignment* (e.g. *SshRepo0*) column in the csv file contains this substring.


**Set the download directory**

It defaults to *output*.

Use the `-o` Flag.

`./repomanager --download -o /tmp/whatever/`

### Import

Run `./repomanager --update --import filename.csv`


**For other flags use the help**

`./repomanager -h`

`./repomanager --help`


## Support
Please create an issue.

## Roadmap

Plans: GUI


## Contributing

**Requirements:**

* Go
	- Required dependencies should be downloaded automatically e.g. if you compile the project.

If you have some good ideas please create an issue.

If you want to implement a new feature please contact me before creating a pull request. (e.g. to avoid that I'm currently working on the same feature)

## License
GNU GPL V3

## Project status

Currently in active development. There is a lot do, especially documentation and tests.
