package main

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/xanzy/go-gitlab"
)

const (
	GuestAccess      = "guest"
	ReporterAccess   = "reporter"
	DeveloperAccess  = "developer"
	MaintainerAccess = "maintainer"
	OwnerAccess      = "owner"
)

type Repository struct {
	HttpURL            string
	SshURL             string
	WebURL             string
	ID                 string
	Members            []*Student
	StudentAccessLevel string

	accessLevel       gitlab.AccessLevelValue
	name              string
	nameWithNamespace string
	tree              []*gitlab.TreeNode
	// upstreamID  int
}

func (r *Repository) AddUser(student *Student) {
	r.Members = append(r.Members, student)
}

func (r *Repository) SetUserAccessLevel(accessLevel string) {
	if accessLevel == GuestAccess {
		r.accessLevel = gitlab.GuestPermissions
	} else if accessLevel == ReporterAccess {
		r.accessLevel = gitlab.ReporterPermissions
	} else if accessLevel == MaintainerAccess {
		r.accessLevel = gitlab.MaintainerPermissions
	} else if accessLevel == OwnerAccess {
		r.accessLevel = gitlab.OwnerPermission
	} else { // DeveloperAccess
		r.accessLevel = gitlab.DeveloperPermissions
	}
}

func (r *Repository) CreateName(team *Team, assignment *Assignment, assignmentName string) error {

	baseName := assignment.RepositoryName

	regex := regexp.MustCompile("<[a-zA-Z]+>")

	fieldsToReplace := regex.FindAllString(baseName, -1)

	replacementMap := make(map[string][]string)

	for _, field := range fieldsToReplace {

		fieldName := strings.ReplaceAll(strings.ReplaceAll(field, "<", ""), ">", "")

		for i := range r.Members {

			if value, err := r.Members[i].GetUnidecodedValueOfMember(fieldName); err == nil {
				replacementMap[field] = append(replacementMap[field], value)
			} else if err != nil {
				return err
			}

		}
	}

	for toReplace, replacement := range replacementMap {
		baseName = strings.ReplaceAll(baseName, toReplace, strings.Join(replacement, "_"))
	}
	baseName = strings.ReplaceAll(baseName, "<Team>", team.GetName())
	baseName = strings.ReplaceAll(baseName, "<Assignment>", assignmentName)
	baseName = strings.ReplaceAll(baseName, " ", "_")

	r.name = baseName

	return nil
}

func (r *Repository) GetName() string {
	return r.name
}

func (r *Repository) GetAccessLevel() *gitlab.AccessLevelValue {
	return &r.accessLevel
}

func (r *Repository) PrintInfo(assignment *Assignment) {

	info, willBeCreated := r.GetInfo(assignment)

	if len(r.ID) > 0 {
		printNotice(info)
	} else if willBeCreated {
		printInfo(info)
	} else {
		printWarning(info)
	}

}

func (r *Repository) GetInfo(assignment *Assignment) (string, bool) {
	forkInfo := fmt.Sprintf("Creating empty repository, upstream is not present (upstream ID = %v)", assignment.UpstreamRepositoryID)
	if assignment.UpstreamRepositoryID > 0 { //FIXME: wrong check
		forkInfo = fmt.Sprintf("Forking from %v", assignment.upstream.HttpURL)
	}

	memberNames := make([]string, len(r.Members))
	memberHasGitlabAccount := false
	for i := range r.Members {
		memberNames[i] = r.Members[i].Name
		if len(r.Members[i].GitlabID) > 0 {
			memberHasGitlabAccount = true
		}
	}
	if len(r.ID) > 0 {
		return fmt.Sprintf("Repository %v already exists", r.name), false
	} else if memberHasGitlabAccount {
		return fmt.Sprintf("Creating repository %v with %v members (%v). %v", r.name, len(r.Members),
			strings.Join(memberNames, " "), forkInfo), true
	} else {
		return fmt.Sprintf("NOT Creating repository %v with %v members (%v). No Gitlab account found.", r.name, len(r.Members),
			strings.Join(memberNames, " ")), false
	}
}

func GetRepositoryKey(assignmentName string) string {
	return RepoPrefix + assignmentName
}

func (r *Repository) GetMemberNames(delimiter string) string {
	m := make([]string, len(r.Members))
	for i := range r.Members {
		m[i] = r.Members[i].Name
	}
	return strings.Join(m, delimiter)
}

func (r *Repository) SetURLs(project *gitlab.Project) {
	r.ID = fmt.Sprint(project.ID)
	r.SshURL = fmt.Sprint(project.SSHURLToRepo)
	r.HttpURL = project.HTTPURLToRepo
	r.WebURL = project.WebURL
}
