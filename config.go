package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/BurntSushi/toml"
)

type Config struct {
	ConfigRepo  *ConfigRepo
	Assignments map[string]*Assignment
	Gitlab      *Gitlab
	// FindGroupRegex []string

	filename string
	git      *Gitlab
	// findGroupRegex []*regexp.Regexp
}

func NewConfig(filename string) (*Config, error) {

	cfg := &Config{filename: filename}

	if _, err := os.Stat(filename); os.IsNotExist(err) || err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil, err
	}

	_, err := toml.DecodeFile(filename, cfg)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil, err
	}

	if cfg.ConfigRepo != nil {
		cfg.git, err = NewGitlab(cfg.ConfigRepo.APIToken, cfg.ConfigRepo.GitlabBaseURL, Args.ConfigBranch)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to initalize config repository")
		}
	}

	for key, assignment := range cfg.Assignments {
		assignment.AccessLevel = strings.ToLower(strings.TrimSpace(assignment.AccessLevel))
		cfg.Assignments[key] = assignment
	}

	return cfg, nil
}

func (cfg *Config) GetGitlabClient() *Gitlab {
	return cfg.git
}

// func ReadConfigFile(filename string) (*Config, error) {

// 	cfg := &Config{filename: filename}

// 	if _, err := os.Stat(filename); os.IsNotExist(err) || err != nil {
// 		fmt.Fprintln(os.Stderr, err)
// 		return nil, err
// 	}

// 	_, err := toml.DecodeFile(filename, cfg)
// 	if err != nil {
// 		fmt.Fprintln(os.Stderr, err)
// 		return nil, err
// 	}

// 	if cfg.ConfigRepo != nil {
// 		cfg.git, err = NewGitlab(cfg.ConfigRepo.APIToken, cfg.ConfigRepo.GitlabBaseURL, Args.ConfigBranch)
// 		if err != nil {
// 			fmt.Fprintln(os.Stderr, "Failed to initalize config repository")
// 		}
// 	}

// 	return cfg, nil
// }

func ReadConfigString(configData string) (*Config, error) {
	cfg := &Config{}

	_, err := toml.Decode(configData, cfg)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	return cfg, nil
}

func (cfg *Config) UpdateConfigFileOnline() error {

	fileContent, err := cfg.getNewConfigData()
	if err != nil {
		return err
	}

	cfg.git.UpdateFile(cfg.ConfigRepo.ConfigRepoID, cfg.ConfigRepo.DatabasePath, fileContent)

	return nil

}

func (cfg *Config) UpdateConfigFileOffline() error {

	fileContent, err := cfg.getNewConfigData()
	if err != nil {
		return err
	}

	err = os.MkdirAll(filepath.Dir(cfg.filename), os.ModePerm)
	if err != nil {
		return err
	}

	file, err := os.Create(cfg.filename)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString(fileContent)

	return err

}

func (cfg *Config) getNewConfigData() (string, error) {

	fileContent := new(bytes.Buffer)
	encoder := toml.NewEncoder(fileContent)

	err := encoder.Encode(cfg)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return "", err
	}

	return fileContent.String(), nil
}

func (cfg *Config) mergeMinAndFullConfig(min *Config, full *Config) {
	full.ConfigRepo = cfg.ConfigRepo
	cfg.Gitlab = full.Gitlab
	cfg.Assignments = full.Assignments
}

func (cfg *Config) GetConfigOffline(filename string) (*Config, error) {

	fullConfig, err := NewConfig(filename)
	if err != nil {
		return nil, err
	}

	cfg.mergeMinAndFullConfig(cfg, fullConfig)

	return fullConfig, nil
}

func (cfg *Config) GetConfigOnline() (*Config, error) {

	fmt.Printf("Get %v from repository with ID %v\n", cfg.ConfigRepo.ConfigPath, cfg.ConfigRepo.ConfigRepoID)

	file, err := cfg.git.DownloadFile(cfg.ConfigRepo.ConfigRepoID, cfg.ConfigRepo.ConfigPath)
	if err != nil {
		return nil, err
	}

	content, err := cfg.git.DecodeFile(file)
	if err != nil {
		return nil, err
	}

	fullConfig, err := ReadConfigString(string(content))
	if err != nil {
		return nil, err
	}

	cfg.mergeMinAndFullConfig(cfg, fullConfig)

	// fullConfig.findGroupRegex = make([]*regexp.Regexp, len(fullConfig.FindGroupRegex))
	// for i := range fullConfig.FindGroupRegex {
	// 	fullConfig.findGroupRegex[i] = regexp.MustCompile(fullConfig.FindGroupRegex[i])
	// }

	// cfg.findGroupRegex = fullConfig.findGroupRegex

	return fullConfig, nil
}
