BINARY_NAME=repomanager
BUILD_DIR=bin

.PHONY: clean run windows linux mac macarm android

desktop: linux windows mac macarm


linux:
	mkdir -p ${BUILD_DIR}
	env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
	go build -o bin/${BINARY_NAME}

windows:
	mkdir -p ${BUILD_DIR}
	env GOOS=windows GOARCH=amd64 CGO_ENABLED=0 \
	go build -o bin/${BINARY_NAME}.exe

mac:
	mkdir -p ${BUILD_DIR}
	env GOOS=darwin GOARCH=amd64 CGO_ENABLED=0 \
	go build -o bin/${BINARY_NAME}_mac

macarm:
	mkdir -p ${BUILD_DIR}
	env GOOS=darwin GOARCH=arm64 CGO_ENABLED=0 \
	go build -o bin/${BINARY_NAME}_mac_arm

android:
	mkdir -p ${BUILD_DIR}
	env GOOS=android GOARCH=arm64 CGO_ENABLED=0 \
	go build -o bin/${BINARY_NAME}.apk

run:
	go run -race $(find . -maxdepth 1 -type f  -iname "*.go"  -exec grep -rL "_test" {} \;)
	#main.go team.go util.go student.go repository.go global.go gitlab.go config.go cliargs.go assignment.go minimalconfig.go

clean:
	go clean
	rm -rf bin/${BINARY_NAME} bin/${BINARY_NAME}.exe bin/${BINARY_NAME}_mac bin/${BINARY_NAME}_mac_arm \
	bin/${BINARY_NAME}.apk

