package main

var Args CLIArgs

const (
	ResetColor   = "\033[0m"    //default
	WarningColor = "\033[1;33m" //Yellow
	ErrorColor   = "\033[31m"   // Red
	InfoColor    = "\033[1;36m" // Very light blue
	InfoColor2   = "\033[33m"
	NoticeColor  = "\033[1;34m" // Purple
	DebugColor   = "\033[0;36m" // Light blue
	Line1        = "-------------------------------------------------------------------------------\n"
	Line2        = "-----------------------------\n"
	DryRunPrefix = "[dry-run] "
)
