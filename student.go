package main

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"github.com/rainycape/unidecode"
)

type Student struct {
	Name            string
	Groups          string
	Group           string
	Teams           map[string]string
	Email           string
	MatriculationNr string
	GitlabID        string
	GitlabUsername  string
	Repos           map[string]*Repository

	filterRepoRegex []*regexp.Regexp
}

func NewStudent() *Student {
	s := &Student{Teams: make(map[string]string), Repos: map[string]*Repository{}}
	for _, pattern := range []string{"(?i)http", "(?i)https", "(?i)ssh", "(?i)id", "(?i)weburl"} {
		s.filterRepoRegex = append(s.filterRepoRegex, regexp.MustCompile(pattern))
	}
	return s
}

func (s *Student) Equals(other *Student) bool {

	matriculationNr, err := strconv.Atoi(s.MatriculationNr)
	matriculationNrOther, err2 := strconv.Atoi(other.MatriculationNr)

	matriculationNrEquals := false

	if err == nil && err2 == nil {
		matriculationNrEquals = (matriculationNr == matriculationNrOther)
	} else {
		matriculationNrEquals = (strings.TrimSpace(s.MatriculationNr) == strings.TrimSpace(other.MatriculationNr))
	}

	return matriculationNrEquals // && strings.TrimSpace(s.Name) == strings.TrimSpace(other.Name)

}

// Update updates all Fields of Student
func (s *Student) Update(newData *Student) {

	s.Email = newData.Email
	s.Groups = newData.Groups

	if len(newData.GitlabID) > 0 {
		s.GitlabID = newData.GitlabID
	}
	if len(newData.GitlabUsername) > 0 {
		s.GitlabUsername = newData.GitlabUsername
	}
	for key, newValue := range newData.Teams {
		if _, hasTeam := s.Teams[key]; !hasTeam {
			s.Teams[key] = newValue
		} else if len(newValue) > 0 {
			// fmt.Printf("Changing Team %v for %v from %v to %v\n", key, s.Name, s.Teams[key], newValue)
			s.Teams[key] = newValue

		}
	}
	for key, newData := range newData.Repos {
		if _, ok := s.Repos[key]; !ok {
			s.Repos[key] = newData
		} else {
			if len(s.Repos[key].SshURL) < len(newData.SshURL) {
				s.Repos[key].SshURL = newData.SshURL
			}
			if len(s.Repos[key].HttpURL) < len(newData.HttpURL) {
				s.Repos[key].HttpURL = newData.HttpURL
			}
			if len(s.Repos[key].ID) < len(newData.ID) {
				s.Repos[key].ID = newData.ID
			}
			if len(s.Repos[key].WebURL) < len(newData.WebURL) {
				s.Repos[key].WebURL = newData.WebURL
			}
		}
	}
}

func (s *Student) ToString(csvHeader []string) string {

	retVal := make([]string, len(csvHeader))

	student := *s
	members := reflect.Indirect(reflect.ValueOf(student))

	for i, columnName := range csvHeader {

		if strings.Contains(strings.ToLower(columnName), "team") {

			retVal[i] = s.Teams[columnName]
		} else if strings.Contains(strings.ToLower(columnName), "repo") {

			repoKey := caseInsensitiveReplace(columnName, "", s.filterRepoRegex) //findrepoRegex.FindString(strings.ToLower(columnName))

			if _, ok := s.Repos[repoKey]; ok {

				if strings.Contains(strings.ToLower(columnName), "ssh") {
					retVal[i] = s.Repos[repoKey].SshURL
				} else if strings.Contains(strings.ToLower(columnName), "http") {
					retVal[i] = s.Repos[repoKey].HttpURL
				} else if strings.Contains(strings.ToLower(columnName), "id") {
					retVal[i] = fmt.Sprint(s.Repos[repoKey].ID)
				} else if strings.Contains(strings.ToLower(columnName), "web") {
					retVal[i] = fmt.Sprint(s.Repos[repoKey].WebURL)
				}
			}
		} else if val, fieldExists := members.Type().FieldByName(columnName); fieldExists {

			if val.Type.Kind() == reflect.String {
				retVal[i] = reflect.ValueOf(&student).Elem().FieldByName(columnName).String()
			}
		}
	}

	return strings.Join(retVal, CSVDelimiter)
}

func (s *Student) GetUnidecodedValueOfMember(memberName string) (string, error) {

	student := *s

	members := reflect.TypeOf(student)

	for i := 0; i < members.NumField(); i++ {
		field := members.Field(i)
		if memberName == field.Name && field.Type.Kind() == reflect.String {
			return unidecode.Unidecode(reflect.ValueOf(&student).Elem().FieldByName(field.Name).String()), nil
		} else if memberName == field.Name {
			return "", nil
		}
	}

	return "", fmt.Errorf("no field with name %v for student", memberName)
}
