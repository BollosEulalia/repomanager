package main

type ConfigRepo struct {
	APIToken      string
	Branch        string
	ConfigPath    string
	DatabasePath  string
	GitlabBaseURL string
	ConfigRepoID  int
}

// func (config *MinimalConfig) NewMinimalConfig(filename string) (*MinimalConfig, error) {

// 	cfg, err := ReadConfigFile(filename)
// 	if err != nil {
// 		fmt.Fprintln(os.Stderr, err)
// 		return nil, err
// 	}

// 	return &cfg.MinimalConfig, nil
// }
