package main

import (
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"
)

func mapKeysToSortedArray(m map[string]any) []string {
	keys := make(map[string]bool, len(m))

	for k := range m {
		keys[k] = true
	}

	sortedKeys := make([]string, 0, len(keys))

	for k := range keys {
		sortedKeys = append(sortedKeys, k)
	}

	sort.Strings(sortedKeys)

	return sortedKeys

}

func reverseName(str string) string {

	name := strings.Split(str, " ")
	length := len(name) - 1

	name[length] = name[length] + LastnameDelimiter
	name[0], name[length] = name[length], name[0] //swap first and last array element

	return strings.Join(name, " ")
}

func reverseGitlabName(str string) string {
	delimIndex := strings.Index(str, LastnameDelimiter)
	retVal := fmt.Sprintf("%v %v", str[delimIndex:], str[:delimIndex])

	return strings.TrimSpace(strings.Replace(retVal, LastnameDelimiter, "", -1))
}

func caseInsensitiveReplace(str string, replacement string, patterns []*regexp.Regexp) string {

	for _, pattern := range patterns {
		str = pattern.ReplaceAllString(str, replacement)
	}

	return str
}

func printNotice(message string, args ...any) {
	printColor(NoticeColor, &message, args...)
}

func printWarning(message string, args ...any) {
	printColor(WarningColor, &message, args...)
}

func printError(message string, args ...any) {
	printColor(ErrorColor, &message, args...)
}

func printInfo(message string, args ...any) {
	printColor(InfoColor, &message, args...)
}

func printColor(color string, message *string, args ...any) {
	fmt.Fprint(os.Stderr, color)
	fmt.Fprintf(os.Stderr, *message, args...)
	fmt.Fprintln(os.Stderr, ResetColor)
}
