package main

import (
	"bytes"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"sort"
	"strings"

	"github.com/jfyne/csvd"
)

const (
	CSVDelimiter    = ";"
	CSVSubDelimiter = ","
	// findRepoColumnString = "repo[0-9]+"
	TeamPrefix = "Team"
	RepoPrefix = "Repo"
)

type Database struct {
	// CurrentData [][]string
	Students []*Student

	configRepo *ConfigRepo
	git        *Gitlab
	// findRepoColumns *regexp.Regexp
	csvHeader   []string
	assignments map[string]*Assignment
}

func NewDatabase(config *Config) (*Database, error) {

	db := &Database{configRepo: config.ConfigRepo, git: config.GetGitlabClient(),
		assignments: config.Assignments} //,

	return db, nil
}

func (db *Database) createCSVHeader() {

	// Only keys are used, values can be ignored
	teamsTemp := make(map[string]interface{}, 0)
	sshURLsTemp := make(map[string]interface{}, 0)
	httpURLsTemp := make(map[string]interface{}, 0)
	webURLsTemp := make(map[string]interface{}, 0)
	repoIDsTemp := make(map[string]interface{}, 0)

	// TODO: use assignments (from config) instead of iterating over students
	for i := range db.Students {
		for t := range db.Students[i].Teams {
			teamsTemp[t] = true
		}
		for r := range db.Students[i].Repos {
			key := r //key := db.findRepoColumns.FindString(strings.ToLower(r))
			if len(key) > 0 {
				sshURLsTemp["Ssh"+key] = true
				httpURLsTemp["Http"+key] = true
				repoIDsTemp[key+"ID"] = true
				webURLsTemp["WebUrl"+key] = true
			}
		}
	}

	teams := mapKeysToSortedArray(map[string]interface{}(teamsTemp))
	sshURLs := mapKeysToSortedArray(map[string]interface{}(sshURLsTemp))
	httpURLs := mapKeysToSortedArray(map[string]interface{}(httpURLsTemp))
	repoIDs := mapKeysToSortedArray(map[string]interface{}(repoIDsTemp))
	webURLs := mapKeysToSortedArray(map[string]interface{}(webURLsTemp))

	header := []string{"Name", "MatriculationNr", "Email", "Groups"}

	header = append(header, teams...)
	header = append(header, webURLs...)
	header = append(header, httpURLs...)
	header = append(header, sshURLs...)
	header = append(header, repoIDs...)
	header = append(header, []string{"GitlabID", "GitlabUsername"}...)

	db.csvHeader = header
	// println(db.csvHeader)

}

func (db *Database) ReadDatabaseOnline() error {

	fmt.Printf("Get %v from repository with ID %v\n", db.configRepo.DatabasePath, db.configRepo.ConfigRepoID)

	file, err := db.git.DownloadFile(db.configRepo.ConfigRepoID, db.configRepo.DatabasePath)

	if err != nil {
		// db.git.ListRepositoryFiles(db.configRepo.ConfigRepoID)
		return err
	}

	fileContent, err := db.git.DecodeFile(file)
	if err != nil {
		return err
	}

	students, err := db.fileContentToStudentArray(fileContent)
	if err != nil {
		return err
	}

	db.Students = students

	return nil

}

func (db *Database) ReadDatabaseOffline(filename string /*, updateStudents bool*/) error {

	students, err := db.getStudentsFromFile(filename)
	if err != nil {
		return err
	}

	db.Students = students

	return nil
}

func (db *Database) getStudentsFromFile(filename string) ([]*Student, error) {

	content, err := os.ReadFile(filename)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not read file %v\n", filename)
		return nil, err
	}

	students, err := db.fileContentToStudentArray(content)
	if err != nil {
		return nil, err
	}

	return students, nil
}

func (db *Database) ImportData(newFile string) error {

	newContent, err := db.getStudentsFromFile(newFile)

	if err != nil {
		return err
	}

	for _, newData := range newContent {
		found := false

		for studentIndex := range db.Students {
			if db.Students[studentIndex].Equals(newData) {
				found = true
				db.Students[studentIndex].Update(newData)
			}
		}
		if !found {
			fmt.Printf("%v not found!\n", newData.Name)
			db.Students = append(db.Students, newData)
		}
	}

	return nil

}

func (db *Database) WriteFile(filename string) error {

	err := os.MkdirAll(filepath.Dir(filename), os.ModePerm)
	if err != nil {
		return err
	}

	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString(db.studentsToCSV())
	return err
}

func (db *Database) studentsToCSV() string {

	db.createCSVHeader()
	// fmt.Println("csvheader=", db.csvHeader)

	csvData := strings.Builder{}

	csvData.WriteString(strings.Join(db.csvHeader, CSVDelimiter))
	csvData.WriteString("\n")

	sort.SliceStable(db.Students, func(i int, j int) bool {
		return db.Students[i].Groups < db.Students[j].Groups
	})

	for _, student := range db.Students {
		csvData.WriteString(student.ToString(db.csvHeader))
		// fmt.Println(student.String(db.csvHeader))
		csvData.WriteString("\n")
	}

	return csvData.String()
}

func (db *Database) fileContentToStudentArray(content []byte) ([]*Student, error) {

	csvReader := csvd.NewReader(bytes.NewReader(content))

	data, err := csvReader.ReadAll()
	if err != nil || len(data) < 1 {
		fmt.Fprintln(os.Stderr, "Could not read content of csv file")
		return nil, err
	}

	students := make([]*Student, 0, len(data))
	// findrepoRegex := db.findRepoColumns

	for r := range data[1:] {
		row := r + 1
		student := *NewStudent()
		studentMembers := reflect.Indirect(reflect.ValueOf(student))

		for assignment := range db.assignments {
			repoKey := GetRepositoryKey(assignment)

			if _, ok := student.Repos[repoKey]; !ok {
				student.Repos[repoKey] = &Repository{}
			}
		}

		for column, columnName := range data[0] {

			// Columns are named Team0, Team1, Team2...
			if strings.Contains(strings.ToLower(columnName), "team") {
				student.Teams[columnName] = data[row][column]
				//Same as Team columns. Columns will be named SshRepo0, HttpRepo0, RepoID0 and so on
			} else if strings.Contains(strings.ToLower(columnName), "repo") {
				for assignment := range db.assignments {
					repoKey := GetRepositoryKey(assignment)

					if _, ok := student.Repos[repoKey]; !ok {
						student.Repos[repoKey] = &Repository{}
					}

					if strings.Contains(strings.ToLower(columnName), strings.ToLower(repoKey)) {
						if strings.Contains(strings.ToLower(columnName), "ssh") {
							student.Repos[repoKey].SshURL = strings.TrimSpace(data[row][column])
						} else if strings.Contains(strings.ToLower(columnName), "http") {
							student.Repos[repoKey].HttpURL = strings.TrimSpace(data[row][column])
						} else if strings.Contains(strings.ToLower(columnName), "id") {
							student.Repos[repoKey].ID = strings.TrimSpace(data[row][column])
						} else if strings.Contains(strings.ToLower(columnName), "web") {
							student.Repos[repoKey].WebURL = strings.TrimSpace(data[row][column])
						}
					}
				}
				// Check if current column of csv is a member of student struct (e.g. Name, MatriuclationNr)
			} else if _, fieldExists := studentMembers.Type().FieldByName(columnName); fieldExists &&
				reflect.ValueOf(&student).Elem().FieldByName(columnName).Kind() != reflect.Map {
				reflect.ValueOf(&student).Elem().FieldByName(columnName).SetString(strings.TrimSpace(data[row][column]))

			}

		}

		students = append(students, &student)

	}

	if len(students) <= 0 {
		fmt.Printf("Student database is empty or could not be read.")
	}

	// sort.SliceStable(students, func(i int, j int) bool {
	// 	return unidecode.Unidecode(students[i].Name) < unidecode.Unidecode(students[j].Name)
	// })

	return students, nil

}

func (db *Database) checkAssignmentsToCreateValid(assignmentsToCreate []string) error {

	configuredAssignmentNames := make([]string, 0, len(db.assignments))
	for a := range db.assignments {
		configuredAssignmentNames = append(configuredAssignmentNames, a)
	}
	sort.Strings(configuredAssignmentNames)

	for _, assignment := range assignmentsToCreate {
		if _, ok := db.assignments[assignment]; !ok {
			return fmt.Errorf("invalid input. Cannot create repository for assignment %v "+
				"Available assignments: %v", assignment, configuredAssignmentNames)
		}
	}
	return nil
}

func (db *Database) FindGroups(regexPatterns []*regexp.Regexp) map[string][]*Student {

	groups := make(map[string][]*Student, len(db.Students))

	for _, student := range db.Students {
		str := student.Groups
		for _, regex := range regexPatterns {
			str = regex.FindString(str)
		}

		// if len(regexPatterns) == 0 {
		// 	str = ""
		// }

		if _, ok := groups[str]; !ok {
			groups[str] = []*Student{student}
		} else {
			groups[str] = append(groups[str], student)
		}

	}

	return groups
}

func (db *Database) CreateTeams(assignmentsToCreate []string) (map[string][]*Team, error) {

	if err := db.checkAssignmentsToCreateValid(assignmentsToCreate); err != nil {
		return nil, err
	}

	padLength := len(fmt.Sprintf("%d", len(db.Students)))
	// println(padLength)
	teamsToCreate := make(map[string]string, len(assignmentsToCreate))
	for _, assignmentName := range assignmentsToCreate {
		teamsToCreate[assignmentName] = TeamPrefix + assignmentName
	}

	existingTeams := make(map[string]*Student, len(db.Students))

	for _, assignmentName := range assignmentsToCreate {

		assignment := db.assignments[assignmentName]
		teamName := teamsToCreate[assignmentName]

		// if len(assignment.Team.SameTeamAs) > 0 {
		// 	db.Students[teamName] = db.Students[assignment.Team.SameTeamAs]
		// }
		// teamName := assignment.Team.SameTeamAs
		// if len(teamName) < 1 {
		// 	teamName = teamsToCreate[assignmentName]
		// }

		for i := range db.Students {
			if val, ok := db.Students[i].Teams[teamName]; ok {
				existingTeams[val] = db.Students[i]
			}
			if len(assignment.Team.SameTeamAs) > 0 {
				db.Students[i].Teams[teamName] = db.Students[i].Teams[assignment.Team.SameTeamAs]
			}
		}

		regexPatterns := make([]*regexp.Regexp, len(assignment.FindGroupRegex))
		for j, regex := range assignment.FindGroupRegex {
			regexPatterns[j] = regexp.MustCompile(regex)
		}

		groups := db.FindGroups(regexPatterns)
		groupKeys := make([]string, 0, len(groups))
		for key := range groups {
			groupKeys = append(groupKeys, key)
		}
		sort.Strings(groupKeys)

		i := -1
		for _, group := range groupKeys {
			for _, student := range groups[group] {

				i++

				// Student has no team yet
				if _, ok := student.Teams[teamName]; !ok || len(student.Teams[teamName]) < 1 {
					teamID := ""

					// Enumerating students because regex is not present
					if assignment.FindTeamRegex == nil || len(assignment.FindTeamRegex) == 0 ||
						len(strings.TrimSpace(assignment.FindTeamRegex[0])) == 0 {

						teamID = fmt.Sprintf("%0*d", padLength, i)
						teamAlreadyExists := true
						tempID := i

						for teamAlreadyExists {
							_, teamAlreadyExists = existingTeams[teamID]
							if teamAlreadyExists {
								tempID = int(math.Max(float64(tempID), float64(len(db.Students))))
								tempID++
								teamID = fmt.Sprintf("%0*d", padLength, tempID)
							}
						}
					} else { // Extracting teamname from groupname(s)

						teamID = student.Groups
						for _, pattern := range assignment.FindTeamRegex {
							regex, err := regexp.Compile(pattern)
							if err != nil {
								return nil, fmt.Errorf("invalid FindTeamRegex in Assignment %v", assignmentName)
							}
							teamID = regex.FindString(teamID)
						}
						// if len(teamID) == 0 {
						// 	return nil, fmt.Errorf("cannot extract team from %v with regex %v for %v",
						// 		student.Groups, assignment.FindTeamRegex, student.Name)
						// }

					}
					// fmt.Printf("%v teamid = %v\n", teamName, teamID)
					student.Teams[teamName] = teamID

				}
			}
		}
	}

	teams := make(map[string]map[string]*Team, len(db.Students))

	for assignment, teamToCreate := range teamsToCreate {
		teamTemplate := db.assignments[assignment].Team
		if _, k := teams[assignment]; !k {
			teams[assignment] = make(map[string]*Team)
		}
		for i, student := range db.Students {
			if val, ok1 := student.Teams[teamToCreate]; ok1 && len(val) > 0 {
				if team, ok := teams[assignment][student.Teams[teamToCreate]]; ok { // Team already exists, adding student

					if err := team.AddMember(student); err != nil {
						return nil, err
					}
				} else {
					teamID := student.Teams[teamToCreate]
					if team, err := NewTeam(teamID, teamTemplate.TeamSize, db.Students[i],
						teamToCreate, GetRepositoryKey(assignment), db.assignments[assignment], assignment); err != nil {
						return nil, err
					} else {

						teams[assignment][teamID] = team
					}

				}
			}
		}
	}

	retVal := make(map[string][]*Team, len(teams))
	for assignment, teamMap := range teams {
		retVal[assignment] = make([]*Team, 0, len(db.Students))
		for _, team := range teamMap {
			retVal[assignment] = append(retVal[assignment], team)
		}
	}

	return retVal, nil
}

func (db *Database) UpdateGitlabIDs() {
	db.git.GetUserIDs(db.Students)
}

func (db *Database) UpdateDatabaseOnline() error {
	return db.git.UpdateFile(db.configRepo.ConfigRepoID, db.configRepo.DatabasePath, db.studentsToCSV())
}

func (db *Database) PadTeamNamesToLength() {
	padTo := len(fmt.Sprint(len(db.Students)))
	for i := range db.Students {
		for teamName, team := range db.Students[i].Teams {
			if len(team) > 0 {
				db.Students[i].Teams[teamName] = fmt.Sprintf("%0*v", padTo, team)
			}
		}
	}
}
