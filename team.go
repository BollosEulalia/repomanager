package main

import "fmt"

type Team struct {
	TeamSize   int
	SameTeamAs string
	members    []*Student
	name       string
	repo       *Repository
}

func NewTeam(name string, size int, student *Student, csvColumn string, repokey string,
	assignment *Assignment, assignmentName string) (*Team, error) {

	team := &Team{name: name, TeamSize: size, members: []*Student{student}, SameTeamAs: csvColumn}

	if val, ok := student.Repos[repokey]; ok {
		team.repo = val
		team.repo.CreateName(team, assignment, assignmentName)
		team.repo.SetUserAccessLevel(assignment.AccessLevel)
		team.repo.AddUser(student)
	}

	return team, nil

}

func (team *Team) AddMember(student *Student) error {

	for i := range team.members { // Student already in team
		if team.members[i].Equals(student) {
			printWarning("%v alreading in team %v [%v]", student.Name, team.name, team.members)
			return nil
		}
	}

	if len(team.members) >= team.TeamSize { //Team too big, some error appeared
		return fmt.Errorf("Team %v already has %v members, can't add %v", team.name, team.TeamSize, student)
	}

	team.members = append(team.members, student)
	team.repo.AddUser(student)

	return nil
}

func (team *Team) GetMembers() []*Student {
	return team.members
}

func (team *Team) GetName() string {
	return team.name
}

func (team *Team) GetRepository() *Repository {
	return team.repo
}

func (team *Team) CreateRepositoryName(assignment *Assignment, assignmentName string) error {

	return team.repo.CreateName(team, assignment, assignmentName)

}
