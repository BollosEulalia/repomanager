package main

import (
	"fmt"
	"math"
	"os"
	"runtime"
	"time"

	"github.com/alexflint/go-arg"
)

type CLIArgs struct {
	Assignments         []string   `arg:"-a, --assignments"`
	Branch              string     `arg:"-b, --branch" default:"main" help:"Branch to download from, will default to branch with latest commit if given branch does not exist."`
	Config              string     `default:"configrepo.toml" help:"path to config file (offline)"`
	ConfigBranch        string     `default:"main" help:"Branch for config repo"`
	CreateRepos         bool       `help:"create repositories for given assignment. Check your settings with --dryrun"`
	Database            string     `help:"path to database (offline)"`
	Deadline            *time.Time `help:"  needs to be in UTC+0 and format 2020-01-20T21:59:59.0000Z"`
	Download            bool       `help:"Downloads files from repositories in group"`
	DryRun              bool       `help:"Do nothing, print what would be done. Doesn't work for --download"`
	Groups              []string   `arg:"-g, --groups" help:"Download assignments only for groups."`
	IgnoreOtherBranches bool       `arg:"--ignore, --ignoreotherbranches" help:"download only from given branch and ignore other branches."`
	Import              string     `help:"import students from given csv into database. Also needs --update flag."`
	NoRename            bool       `help:"do not append student names to downloaded repositories"`
	OutDir              string     `arg:"-o, --outdir" default:"output"`
	Search              []string   `help:"search string for gitlab api. needs to be at least 3 characters long"`
	Threads             int        `arg:"-t, --threads" help:"number of threads. Defaults to cpu_threads / 2"`
	Tree                []string   `help:"path to search in git tree. Use if you want just a subfolder e.g. a4"`
	TimeoutOnError      int64      `arg:"-w" default:"5" help:"If an error occurs (e.g. too many requests on gitlab) program will wait for x seconds"`
	Update              bool       `arg:"-u, --update" help:"updates the database, e.g. if there are missing gitlab IDs for students it checks if they exist now and updates the database."`
	FullConfig          string
	SpecialDeadlines    map[string]time.Time `help:"set deadlines per repository. repositoryID=Deadline example: 1900=2022-04-30T21:59:59.0000Z"`
	useOfflineDatabase  bool
}

// var Args CLIArgs
// parseArgs parses passed CLI arguments and sets some default values which are not supported by go-arg.
func parseArgs(args *CLIArgs, cfg ...*Config) error {

	arg.MustParse(args)

	if args.Threads < 1 {
		args.Threads = int(math.Max(float64(runtime.NumCPU()/2), 1))
	}
	//minimum 1 second, default 5 seconds
	args.TimeoutOnError = int64(math.Max(float64(args.TimeoutOnError)*float64(time.Second), float64(time.Second)))

	if args.Search == nil || len(args.Search) == 0 {
		args.Search = []string{""}
	}

	if args.Groups == nil || len(args.Groups) == 0 {
		args.Groups = []string{""}
	}

	if args.CreateRepos && (args.Assignments == nil || len(args.Assignments) == 0) {
		fmt.Printf("Error. Usage: ./%v --createrepos -a <assignment>\n", os.Args[0])
		os.Exit(2)
	}

	if args.SpecialDeadlines == nil {
		args.SpecialDeadlines = make(map[string]time.Time)
	} else {
		for repoID, deadline := range args.SpecialDeadlines {
			fmt.Printf("Deadline for %v = %v\n", repoID, deadline)
		}
	}

	args.useOfflineDatabase = len(args.Database) > 0

	return nil

}

func (args *CLIArgs) SetBranchAndDeadline(cfg *Config) {

	var assignment *Assignment
	ok := false
	if len(args.Assignments) > 0 {
		assignment, ok = cfg.Assignments[args.Assignments[0]]
	}

	if ok && len(assignment.SubmissionBranch) > 0 && !argvContains([]string{"-b", "--branch"}) {
		args.Branch = assignment.SubmissionBranch
	}
	if ok && len(args.Tree) < 1 && len(assignment.Subfolders) > 0 {
		args.Tree = assignment.Subfolders
	}

	if args.Download && args.Deadline == nil {

		if ok && assignment.Deadline != nil {
			args.Deadline = assignment.Deadline
		} else {
			now := time.Now()
			args.Deadline = &now
		}

	}

	if args.Download {
		printInfo("\nDeadline set to %v (%v)", Args.Deadline.UTC(),
			Args.Deadline.Local())
		printInfo("Branch set to %v", Args.Branch)
	}

	if len(args.Assignments) > 1 {
		printWarning("only 1 assignment possible for downloading. Using Assignment '%v'", args.Assignments[0])
	}
	println("")
}

func argvContains(arg []string) bool {
	for i := range os.Args {
		for j := range arg {
			if os.Args[i] == arg[j] {
				return true
			}
		}
	}
	return false
}
