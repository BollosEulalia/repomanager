package main

import (
	"encoding/base32"
	"encoding/base64"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/alitto/pond"
	glob "github.com/ganbarodigital/go_glob"
	"github.com/rainycape/unidecode"
	"github.com/xanzy/go-gitlab"
)

const (
	AutoAuthorName     = "repomanager"
	UpdateFileMessage  = "auto-update"
	LastnameDelimiter  = ","
	MaxThreadqueueSize = 10000
	ErrNameTaken       = 409
)

type ProjectData struct {
	Project *gitlab.Project
	Members []*gitlab.ProjectMember
}

type GitlabUserdata struct {
	Username string
	ID       int
}

type Download struct {
	SubmissionBranch string
	ExcludedFiles    []string
}

type Gitlab struct {
	APIToken string
	BaseURL  string
	GroupID  int

	FilesToDownload          []string
	ExcludeFilesFromDownload []string

	branch string
	client *gitlab.Client
}

func NewGitlab(apiToken string, baseURL string, defaultBranch string) (*Gitlab, error) {

	var err error
	git := &Gitlab{}

	git.client, err = gitlab.NewClient(apiToken, gitlab.WithBaseURL(baseURL))
	if err != nil || git.client == nil {
		fmt.Fprintf(os.Stderr, "Could not initialize gitlab client at %v. Check your token.\n", baseURL)
		return nil, err
	}

	if len(defaultBranch) > 0 {
		git.branch = defaultBranch
	}

	return git, nil
}

func (git *Gitlab) Init(assignments map[string]*Assignment, baseGit *Gitlab) error {

	var err error
	var project *gitlab.Project

	git.GroupID = baseGit.GroupID
	git.FilesToDownload = baseGit.FilesToDownload
	git.ExcludeFilesFromDownload = baseGit.ExcludeFilesFromDownload

	for assignmentName, assignment := range assignments {
		if assignment.UpstreamRepositoryID > 0 {
			fmt.Printf("Getting upstream repository for assignment %v\n", assignmentName)
			project, _, err = git.client.Projects.GetProject(assignment.UpstreamRepositoryID, nil)
		} else if assignment.UpstreamRepositoryID != -1 {
			return fmt.Errorf("upstream not configured correctly for assignment %v\nConfigure UpstreamRepositoryID to a real ID or set it to -1 if you don't need an upstream and want to create an empty repository", assignmentName)
		}
		if err != nil {
			fmt.Fprintf(os.Stderr, "Upstream not configured correctly for assignment %v\n", assignmentName)
			return err
		}
		if project != nil {

			tree, err := git.GetTree(project.ID, nil, nil)
			if err != nil {
				return err
			}

			assignment.upstream = &Repository{name: project.Name, nameWithNamespace: project.NameWithNamespace,
				ID: fmt.Sprint(project.ID), SshURL: project.SSHURLToRepo, HttpURL: project.HTTPURLToRepo,
				StudentAccessLevel: assignment.AccessLevel, tree: tree}

			assignments[assignmentName] = assignment

		}
	}

	if _, _, err = git.client.Groups.GetGroup(git.GroupID, nil); err != nil {
		printWarning("Gitlab GroupID not configured correctly for assignment\nConfigured ID = %v\n", git.GroupID)
		return err
	}

	if len(git.FilesToDownload) < 1 {
		git.FilesToDownload = []string{"*"}
	}

	return nil
}

func (git *Gitlab) ListRepositoryFiles(repositoryID interface{}) {
	tree, _, _ := git.client.Repositories.ListTree(repositoryID, nil)

	for i := range tree {
		fmt.Printf("%v", tree[i])
	}
}

func (git *Gitlab) DownloadFile(repositoryID int, filename string) (*gitlab.File, error) {

	file, _, err := git.client.RepositoryFiles.GetFile(repositoryID, filename,
		&gitlab.GetFileOptions{Ref: &git.branch})
	if err != nil {
		printWarning("Failed to get file '%v' from repository '%v'\n", filename, repositoryID)

		return nil, err
	}

	return file, nil
}

// Decodes file based on its encoding.
// According to gitlab api documentation encoding should always be base64
func (git *Gitlab) DecodeFile(file *gitlab.File) ([]byte, error) {

	content := make([]byte, 0)
	var err error

	switch file.Encoding {
	case "base64":
		content, err = base64.StdEncoding.DecodeString(file.Content)
	case "base32":
		content, err = base32.StdEncoding.DecodeString(file.Content)
	}

	return content, err
}

func (git *Gitlab) UpdateFile(repositoryID int, filename string, content string) error {

	options := &gitlab.UpdateFileOptions{Content: &content,
		AuthorName:    gitlab.String(AutoAuthorName),
		CommitMessage: gitlab.String(fmt.Sprintf("%v %v", UpdateFileMessage, filename)),
		Branch:        &git.branch}

	_, _, err := git.client.RepositoryFiles.UpdateFile(repositoryID, filename, options)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to update %v. %v", filename, err.Error())
		return err
	}

	// gitlab.decode

	return nil
}

func (git *Gitlab) combineFoundUsersAndStudents(students []*Student, userData map[int][]*gitlab.User) {

	for i, gitlabUsers := range userData {

		if len(gitlabUsers) == 1 {
			students[i].GitlabID = fmt.Sprintf("%v", gitlabUsers[0].ID)
			students[i].GitlabUsername = gitlabUsers[0].Username
		} else {

			userMatched := false
			// gitlabID, err := strconv.Atoi(students[i].GitlabID)

			// if err == nil {
			for _, gitlabUser := range gitlabUsers {

				if students[i].GitlabID == fmt.Sprint(gitlabUser.ID) ||
					students[i].GitlabUsername == gitlabUser.Username { //students[i].GitlabID == gitlabUser.ID
					students[i].GitlabID = fmt.Sprintf("%v", gitlabUsers[0].ID)
					students[i].GitlabUsername = gitlabUsers[0].Username
					userMatched = true
					break
				}
			}
			// }

			if !userMatched {
				printWarning("Multiple users found for student %v. Please choose one.\n", students[i].Name)
				// fmt.Printf("Multiple users found for student %v. Please choose one.\n", students[i].Name)
				count := 1
				for ; count <= len(gitlabUsers); count++ {
					u := gitlabUsers[count-1]
					s := students[i]
					fmt.Printf("%s(%v)%s gitlab-mail: %v;gitlab-publicmail: %v;%s gitlab-ID: %v; username: %v;%s\n"+
						"    account created at: %v;last activity: %v;last sign in: %v;url: %v, %sgitlab name: %v;%s\n"+
						"  %s[Current infos in database: gitlabID: %v, gitlabUsername: %v, name: %v]%s\n",
						InfoColor2, count, ResetColor, u.Email, u.PublicEmail,
						InfoColor, u.ID, u.Username, ResetColor,
						u.CreatedAt, u.LastActivityOn, u.LastSignInAt, u.WebURL,
						InfoColor, u.Name, ResetColor,
						InfoColor, s.GitlabID, s.GitlabUsername, s.Name, ResetColor)
					fmt.Print(Line2)
				}

				fmt.Printf("(%v) None\n", count)
				var err error = errors.New("")
				input := 0
				for err != nil {
					_, err = fmt.Scanf("%d", &input)
					if err != nil && input < 1 && input > count {
						err = errors.New("invalid input")
					}
				}
				// TODO: testing
				//if input == count then "None" was chosen.
				if input != count {
					students[i].GitlabID = fmt.Sprintf("%v", gitlabUsers[input-1].ID)
					students[i].GitlabUsername = gitlabUsers[input-1].Username
				}
			}
		}
	}
}

// Returns an array containg the indexes of all students without a gitlab ID
func (git *Gitlab) usersWithoutID(students []*Student) []int {

	withoutID := make([]int, 0, len(students))
	for i := range students {
		if len(students[i].GitlabID) < 1 || len(students[i].GitlabUsername) < 1 {
			withoutID = append(withoutID, i)
		}
	}
	return withoutID
}

func (git *Gitlab) GetUserIDs(students []*Student) {

	// key = array index, value = found users returned by gitlab
	userData := make(map[int][]*gitlab.User, len(students))
	mutex := sync.Mutex{}
	dataLock := sync.Mutex{}

	pool := pond.New(Args.Threads, len(students))

	studentsWithoutID := git.usersWithoutID(students)
	fmt.Printf("Students without gitlab ID: %v\n", len(studentsWithoutID))
	padSize := len(fmt.Sprintf("%d", len(studentsWithoutID)))
	done := 0

	for _, i := range studentsWithoutID {

		index := i
		student := students[i]
		client := git.client

		pool.Submit(func() {

			var usersFound []*gitlab.User
			err := errors.New("")

			for err != nil { // while err != nil

				reversedName := reverseName(student.Name) //For some users you need to search with "lastname, firstname" to find them

				usersFound, _, err = client.Users.ListUsers(
					&gitlab.ListUsersOptions{Search: gitlab.String(reversedName)}) //TODO: find actual difference between search.Users() and Users.ListUsers()

				if err != nil || usersFound == nil || len(usersFound) <= 0 {

					usersFound, _, err = client.Users.ListUsers(
						&gitlab.ListUsersOptions{Search: &student.Name})
				}

				if err == nil && usersFound != nil && len(usersFound) > 0 {

					matchingUsers := make([]*gitlab.User, 0, len(usersFound))
					for _, user := range usersFound { //Check if the name matches 100%
						if student.Name == reverseGitlabName(user.Name) {
							matchingUsers = append(matchingUsers, user)
						}
					}
					dataLock.Lock()

					userData[index] = matchingUsers
					dataLock.Unlock()

				} else if err != nil {
					time.Sleep(time.Duration(Args.TimeoutOnError))
				}
			}

			mutex.Lock()
			done++
			fmt.Printf("\rUser IDs fetched: %0*d/%d", padSize, done, len(studentsWithoutID))
			mutex.Unlock()

		})

	}

	pool.StopAndWait()
	println("")
	git.combineFoundUsersAndStudents(students, userData)

}

func (git *Gitlab) GetCommits(repoID interface{}, branch string, deadline *time.Time, client1 ...*gitlab.Client) ([]*gitlab.Commit, error) {

	listAllCommits := true

	client := git.client

	if len(client1) > 0 {
		client = client1[0]
	}

	commits := make([]*gitlab.Commit, 0, 100)

	listCommitOptions := &gitlab.ListCommitsOptions{
		RefName: &branch, All: &listAllCommits, Until: deadline,
		ListOptions: gitlab.ListOptions{Page: 1, PerPage: 100}}

	for {

		commit, resp, _ := client.Commits.ListCommits(repoID, listCommitOptions)

		if commit != nil {
			commits = append(commits, commit...)
		}

		listCommitOptions.Page = resp.NextPage

		if resp.NextPage == 0 {
			break
		}
	}

	sort.SliceStable(commits, func(i int, j int) bool {
		return commits[i].CreatedAt.After(*commits[j].CreatedAt)
	})

	if len(commits) < 1 {
		return nil, fmt.Errorf("no commits found in repository %v", repoID)
	}

	return commits, nil
}

func (git *Gitlab) GetMostRecentBranch(repoID interface{}, commit *string, path *string, deadline *time.Time,
	client1 ...*gitlab.Client) (*gitlab.Branch, error) {

	allBranches, err := git.GetBranches(repoID, commit, path, client1...)
	if err != nil {
		return nil, err
	}

	latestBranch := allBranches[0]
	mostRecentCommitDate := latestBranch.Commit.CreatedAt

	for i := range allBranches {
		commits, err := git.GetCommits(repoID, allBranches[i].Name, deadline, client1...)
		if err == nil && commits != nil && commits[0].CreatedAt.After(*mostRecentCommitDate) {
			mostRecentCommitDate = commits[0].CreatedAt
			latestBranch = allBranches[i]
		}
	}

	return latestBranch, nil
}

// Returns the branch wanted or if it doesn't exist it returns the branch with the most recent commit
func (git *Gitlab) GetWantedOrMostRecentBranch(repoID interface{}, branchName string, commit *string, path *string,
	deadline *time.Time, client1 ...*gitlab.Client) (*gitlab.Branch, error) {

	client := git.client
	if len(client1) > 0 {
		client = client1[0]
	}

	branch, _, err := client.Branches.GetBranch(repoID, branchName)
	if err == nil && branch != nil {
		return branch, nil
	}

	mostRecentBranch, err := git.GetMostRecentBranch(repoID, commit, path, deadline, client1...)
	if err != nil {
		return nil, err
	} else if mostRecentBranch == nil {
		return nil, fmt.Errorf("no Branch found in repository %v", repoID)
	}

	return mostRecentBranch, nil
}

func (git *Gitlab) GetBranches(repoID interface{}, commit *string, path *string,
	client1 ...*gitlab.Client) ([]*gitlab.Branch, error) {

	client := git.client

	if len(client1) > 0 {
		client = client1[0]
	}

	allBranches := make([]*gitlab.Branch, 0, 100)
	listBranchOptions := &gitlab.ListBranchesOptions{
		ListOptions: gitlab.ListOptions{Page: 1, PerPage: 100}}

	for {

		branches, resp, _ := client.Branches.ListBranches(repoID, listBranchOptions)

		if branches != nil {
			allBranches = append(allBranches, branches...)
		}

		listBranchOptions.Page = resp.NextPage

		if resp.NextPage == 0 {
			break
		}
	}

	return allBranches, nil
}

func (git *Gitlab) GetTree(repoID interface{}, commit *string, path *string, client1 ...*gitlab.Client) ([]*gitlab.TreeNode, error) {

	recursive := true
	mutex := sync.Mutex{}

	client := git.client

	if len(client1) > 0 {
		client = client1[0]
	}

	treeListOptions1 := &gitlab.ListTreeOptions{Recursive: &recursive, Ref: commit, Path: path,
		ListOptions: gitlab.ListOptions{Page: 1, PerPage: 100}}

	tree1, resp, err := client.Repositories.ListTree(repoID, treeListOptions1)
	if err != nil {
		return nil, err
	}

	totalPages := resp.TotalPages

	fullTree := make([]*gitlab.TreeNode, 0, totalPages*resp.ItemsPerPage)
	fullTree = append(fullTree, tree1...)

	for page := 2; page <= totalPages; page++ {

		treeListOptions := &gitlab.ListTreeOptions{Recursive: &recursive, Ref: commit, Path: path,
			ListOptions: gitlab.ListOptions{Page: page, PerPage: 100}}

		tree, _, err := client.Repositories.ListTree(repoID, treeListOptions)
		if err != nil {
			return nil, err
		}
		mutex.Lock()
		fullTree = append(fullTree, tree...)
		mutex.Unlock()
	}

	return fullTree, nil
}

func (git *Gitlab) checkForkOk(createdRepository *gitlab.Project, upstream *Repository, client1 ...*gitlab.Client) bool {

	client := git.client
	if len(client1) > 0 {
		client = client1[0]
	}

	var tree []*gitlab.TreeNode

	for j := 0; j < 5; j++ {
		var e error
		time.Sleep(time.Second * 4)
		if tree, e = git.GetTree(createdRepository.ID, nil, nil, client); e == nil {
			if e == nil && len(tree) >= len(upstream.tree) {
				return true
			}
		}
	}
	return false
}

func (git *Gitlab) CreateRepositories(assignments map[string]*Assignment, teams map[string][]*Team) error {

	// reportLock := sync.Mutex{}
	// report := strings.Builder{}

	for assignmentName, currentTeams := range teams {

		pool := pond.New(Args.Threads, len(currentTeams))

		for i := range currentTeams {

			assignment := assignments[assignmentName]
			team := currentTeams[i]
			repo := team.GetRepository()
			upstreamID := assignment.UpstreamRepositoryID
			upstream := assignment.upstream
			client := *git.client

			forkOptions := gitlab.ForkProjectOptions{NamespaceID: &git.GroupID,
				Name:       gitlab.String(repo.GetName()),
				Path:       gitlab.String(repo.GetName()),
				Visibility: gitlab.Visibility(gitlab.PrivateVisibility)}

			createEmptyOptions := gitlab.CreateProjectOptions{NamespaceID: &git.GroupID,
				Name:       gitlab.String(repo.GetName()),
				Path:       gitlab.String(repo.GetName()),
				Visibility: gitlab.Visibility(gitlab.PrivateVisibility)}

			if !Args.DryRun {

				pool.Submit(func() {

					var createdRepository *gitlab.Project
					var creationErr error
					var resp *gitlab.Response
					printMessage := strings.Builder{}

					info, willBeCreated := repo.GetInfo(assignment)
					if willBeCreated {
						printMessage.WriteString(fmt.Sprintf("%s%v%s\n", InfoColor, info, ResetColor))
					} else {
						printMessage.WriteString(fmt.Sprintf("%s%v%s\n", WarningColor, info, ResetColor))
					}

					if len(repo.ID) < 1 { //repository does not exist or id is not in database

						// repo.PrintInfo(assignment)

						creationSuccessful := false

						for i := 0; i < 3 && !creationSuccessful; i++ {

							if assignment.UpstreamRepositoryID > 0 { // fork repository
								createdRepository, resp, creationErr = client.Projects.ForkProject(upstreamID, &forkOptions)

								// check if forked repo has at least the same amount of files
								// if not the easiest fix is to just delete the repository and create a new one
								if creationErr == nil {
									creationSuccessful = git.checkForkOk(createdRepository, upstream, &client)

									if !creationSuccessful && time.Since(*createdRepository.CreatedAt) < time.Minute {
										client.Projects.DeleteProject(createdRepository.ID)
									}
								}
							} else { //create empty repository (no upstream available)
								createdRepository, resp, creationErr = client.Projects.CreateProject(&createEmptyOptions)
							}

						}
						// Repository already exists
						if resp.StatusCode == ErrNameTaken {
							repos, _, e := client.Groups.ListGroupProjects(git.GroupID,
								&gitlab.ListGroupProjectsOptions{Search: gitlab.String(repo.GetName())})
							for r := range repos {
								if repos[r].Name == repo.GetName() {
									createdRepository = repos[r]
									creationErr = e
									break
								}
							}

						}

						if creationErr != nil {
							// printError(creationErr.Error())
							printMessage.WriteString(fmt.Sprintf("    %s%v%s\n", ErrorColor, creationErr.Error(), ResetColor))

						}
						repo.SetURLs(createdRepository)

					}

					git.AddMembersToRepository(repo, &client, &printMessage)

					fmt.Println(printMessage.String())

				})

			} else {

				fmt.Print(DryRunPrefix, " ")
				repo.PrintInfo(assignment)
			}

		}

		pool.StopAndWait()

	}

	return nil
}

func (git *Gitlab) AddMembersToRepository(repository *Repository, client *gitlab.Client,
	printMessage ...*strings.Builder) error { //, client *gitlab.Client) error {

	for _, member := range repository.Members {
		_, _, err := client.ProjectMembers.AddProjectMember(repository.ID,
			&gitlab.AddProjectMemberOptions{UserID: member.GitlabID, AccessLevel: repository.GetAccessLevel()})
		if err != nil {
			if len(printMessage) > 0 {
				printMessage[0].WriteString(fmt.Sprintf("%sError adding %v to %v\n  %v\n%s",
					ErrorColor, member.Name, repository.name, err.Error(), ResetColor))
			}
			// printWarning(err.Error())
			return err
		} else {
			if len(printMessage) > 0 {
				printMessage[0].WriteString(fmt.Sprintf("Added %v to %v\n  %v\n",
					member.Name, repository.name, err.Error()))
			}
		}
	}
	return nil
}

func (git *Gitlab) GetProjectMembers(projects []*gitlab.Project, client1 ...*gitlab.Client) ([]ProjectData, error) {

	projectsWithMembers := make([]ProjectData, len(projects))

	pool := pond.New(Args.Threads, len(projects))

	projectLock := sync.Mutex{}
	mutex := sync.Mutex{}

	done := 0
	all := len(projects)

	for p := range projects {

		index := p
		project := *projects[p]
		client := *git.client
		if len(client1) > 0 {
			client = *client1[0]
		}
		print := len(client1) < 1

		pool.Submit(func() {
			projectMembers, _, err := client.ProjectMembers.ListProjectMembers(project.ID,
				&gitlab.ListProjectMembersOptions{ListOptions: gitlab.ListOptions{PerPage: 100}})
			if err != nil {
				printError(err.Error())
			}

			projectLock.Lock()
			projectsWithMembers[index] = ProjectData{Project: projects[index], Members: projectMembers}
			projectLock.Unlock()
			mutex.Lock()
			done++
			if print {
				fmt.Printf("\rGetting Project Members for all projects: %0*d/%d", len(fmt.Sprint(all)), done, all)
			}
			mutex.Unlock()

		})
	}

	pool.StopAndWait()
	if len(client1) < 1 {
		println("")
	}

	return projectsWithMembers, nil
}

func (git *Gitlab) ProjectMembersEqualsTeamMembers(projectData *ProjectData,
	team *Team) bool {

	return fmt.Sprint(projectData.Project.ID) == team.repo.ID ||
		projectData.Project.SSHURLToRepo == team.repo.SshURL ||
		projectData.Project.HTTPURLToRepo == team.repo.HttpURL ||
		projectData.Project.WebURL == team.repo.WebURL ||
		team.repo.name == projectData.Project.Name

}

//CombineProjectsAndTeams checks which project belongs to which team.
// teams has assignmentName as Key and all teams for that assignment as value.
func (git *Gitlab) CombineProjectsAndTeams(projects []*gitlab.Project, teams map[string][]*Team) {

	projectsWithMembers, _ := git.GetProjectMembers(projects)

	for assignmentName := range teams {
		println(assignmentName)
		for teamIndex := range teams[assignmentName] {

			for projectIndex := range projectsWithMembers {

				if git.ProjectMembersEqualsTeamMembers(&projectsWithMembers[projectIndex], teams[assignmentName][teamIndex]) {
					teams[assignmentName][teamIndex].repo.SetURLs(projectsWithMembers[projectIndex].Project)
				}

			}
		}
	}
}

func (git *Gitlab) GetProjects(teams map[string][]*Team, search []string) ([]*gitlab.Project, error) {

	mutex := sync.Mutex{}
	projectLock := sync.Mutex{}
	foundProjects := make([]*gitlab.Project, 0, 1000)

	for s := range search {

		println("Getting projects for search ", search[s])
		pool := pond.New(Args.Threads, MaxThreadqueueSize)

		searchOptions1 := gitlab.ListGroupProjectsOptions{Search: gitlab.String(search[s]),
			ListOptions: gitlab.ListOptions{PerPage: 100, Page: 1}}

		projects1, resp1, err1 := git.client.Groups.ListGroupProjects(git.GroupID, &searchOptions1)
		if err1 != nil {
			return nil, err1
		}

		done := 1

		foundProjects = append(foundProjects, projects1...)

		totalPages := resp1.TotalPages

		for page := 2; page <= totalPages; page++ {

			client := *git.client
			currentPage := page

			searchOptions := gitlab.ListGroupProjectsOptions{Search: gitlab.String(search[s]),
				ListOptions: gitlab.ListOptions{PerPage: 100, Page: currentPage}}

			pool.Submit(func() {
				projects, _, err := client.Groups.ListGroupProjects(git.GroupID, &searchOptions)
				if err != nil {
					printError(err.Error())
				}

				projectLock.Lock()
				foundProjects = append(foundProjects, projects...)
				projectLock.Unlock()

				mutex.Lock()
				done++
				fmt.Printf("\rProject pages fetched: %0*d/%d", len(fmt.Sprint(totalPages)), done, totalPages)
				mutex.Unlock()

			})

		}

		pool.StopAndWait()
		println("")
	}

	return foundProjects, nil

}

func (git *Gitlab) FileMatchesWantedPattern(filename string, wantedTrees []string) bool {

	for _, treename := range wantedTrees {
		if !strings.HasPrefix(filename, treename) {
			return false
		}
	}

	for _, excludeFilename := range git.ExcludeFilesFromDownload {
		matched, _ := glob.NewGlob(excludeFilename).Match(filename)
		if matched {
			return false
		}
	}

	for _, wantedFilename := range git.FilesToDownload {

		matched, _ := glob.NewGlob(wantedFilename).Match(filename)
		if matched {
			return true
		}

	}

	return false
}

// func (git *Gitlab) DownloadFiles()

func (git *Gitlab) writeCommitFile(filename string, commits []*gitlab.Commit) error {

	content := strings.Builder{}

	for _, c := range commits {
		commit := []string{c.CreatedAt.UTC().String(), c.CommitterName, c.CommitterEmail, c.AuthorEmail, c.ID, c.Message, c.CommittedDate.UTC().String()}
		content.WriteString(strings.Join(commit, CSVDelimiter) + "\n")

	}

	return os.WriteFile(filename, []byte(content.String()), fs.ModePerm)

}

func (git *Gitlab) writeMemberFile(filename string, members []*gitlab.ProjectMember) error {

	content := strings.Builder{}

	for i := range members {
		member := []string{members[i].Name, fmt.Sprint(members[i].ID), members[i].Username, members[i].Email}
		content.WriteString(strings.Join(member, CSVDelimiter) + "\n")
	}
	return os.WriteFile(filename, []byte(content.String()), os.ModePerm)

}

func (git *Gitlab) getMemberNames(members []*gitlab.ProjectMember, delim string,
	whitespaceReplacement ...string) string {

	retVal := make([]string, len(members))
	replacement := "_"
	if len(whitespaceReplacement) > 0 {
		replacement = whitespaceReplacement[0]
	}

	for i := range members {
		retVal[i] = unidecode.Unidecode(strings.ReplaceAll(
			strings.ReplaceAll(members[i].Name, ",", ""), " ", replacement))
	}

	return strings.Join(retVal, delim)
}

func (git *Gitlab) createDownloadInfoCSVColumn(project *gitlab.Project, branch *gitlab.Branch,
	commits []*gitlab.Commit, members []*gitlab.ProjectMember,
	repo *Repository, filenames []string, path string) string {

	gitMemberNames := make([]string, len(members))
	for i := range members {
		gitMemberNames[i] = reverseGitlabName(members[i].Name)
	}

	lines := make([]string, len(repo.Members))

	for i := range repo.Members {
		lines[i] = strings.ReplaceAll(
			strings.Join(
				[]string{
					repo.Members[i].Name,
					repo.Members[i].MatriculationNr,
					repo.Members[i].Email,
					project.Name,
					repo.GetMemberNames(CSVSubDelimiter),
					strings.Join(gitMemberNames, CSVSubDelimiter), // git.getMemberNames(members, CSVSubDelimiter, " "),
					branch.Name,
					commits[0].CreatedAt.UTC().String(),
					commits[0].CommitterName,
					commits[0].ID,
					strings.ReplaceAll(commits[0].Message, CSVDelimiter, ""),
					strings.Join(filenames, CSVSubDelimiter),
					project.HTTPURLToRepo,
					project.SSHURLToRepo,
					filepath.Base(path),
				},
				CSVDelimiter),
			"\n", "")

	}

	return strings.Join(lines, "\n")

}

func (git *Gitlab) DownloadSubmissions(repositories []*Repository, args *CLIArgs) error {

	println("Starting Downloads...")

	pool := pond.New(args.Threads, len(repositories))
	fileLock := sync.Mutex{}
	mutex := sync.Mutex{}
	report := make([]string, len(repositories)+1)
	report[0] = strings.Join([]string{
		"Student",
		"MatriculationNr",
		"Email",
		"Repository Name",
		"Members(Repolist)",
		"Members(Gitlab)",
		"Branch",
		"CommitDate",
		"CommitAuthor",
		"CommitID",
		"CommitMessage",
		"Files Downloaded",
		"SSH-URL",
		"HTTP-URL",
		"Directory Name",
	}, CSVDelimiter)

	done := 0
	padTo := len(fmt.Sprint(len(repositories)))
	all := len(repositories)

	for i := range repositories {

		wantedTrees := args.Tree
		repo := repositories[i]
		client := *git.client
		wantedBranch := args.Branch

		deadline := args.Deadline
		if dl, ok := args.SpecialDeadlines[repo.ID]; ok {
			deadline = &dl
		}
		// outDir := args.OutDir
		currentDir := filepath.Join(args.OutDir, repo.Members[0].Group)
		// var downloadedFiles []string
		rename := !args.NoRename
		reportIndex := i + 1
		ignoreOtherBranches := args.IgnoreOtherBranches

		pool.Submit(func() {

			project, _, err := client.Projects.GetProject(repo.ID, &gitlab.GetProjectOptions{})
			if err != nil {
				return // TODO: error message
			}

			currentDir = filepath.Join(currentDir, project.Name)

			branch, err := git.GetWantedOrMostRecentBranch(repo.ID, wantedBranch, nil, nil, deadline, &client)
			if err != nil {
				return //TODO: write error message
			}

			commits, err := git.GetCommits(repo.ID, branch.Name, deadline, &client)
			if err != nil {
				return // TODO: error message
			}

			tree, err := git.GetTree(repo.ID, &commits[0].ID, nil, &client)
			if err != nil {
				return // TODO: error message
			}
			projectData, err := git.GetProjectMembers([]*gitlab.Project{project}, &client)
			if err != nil {
				return // TODO: error message
			}

			mutex.Lock()
			downloadedFiles := make([]string, 0, len(tree))
			mutex.Unlock()

			if rename {
				currentDir = currentDir + "_" + git.getMemberNames(projectData[0].Members, "-")
			}

			if !ignoreOtherBranches || (ignoreOtherBranches && branch.Name == wantedBranch) {
				for t := range tree {
					// Check if tree[t] is a file and save it if it matches the pattern from config file
					if tree[t].Type != "tree" && git.FileMatchesWantedPattern(tree[t].Path, wantedTrees) {
						filename := filepath.Join(currentDir, tree[t].Path)
						fileLock.Lock()
						if err = os.MkdirAll(filepath.Dir(filename), os.ModePerm); err == nil {
							if f, _, err := client.RepositoryFiles.GetFile(repo.ID, tree[t].Path,
								&gitlab.GetFileOptions{Ref: &commits[0].ID}); err == nil {
								if content, err := git.DecodeFile(f); err == nil {
									if err = os.WriteFile(filename, content, os.ModePerm); err != nil {
										//TODO: error message
										println("")
									} else {
										mutex.Lock()
										if len(strings.TrimSpace(tree[t].Path)) > 0 {
											downloadedFiles = append(downloadedFiles, tree[t].Path)
										}
										mutex.Unlock()
									}
								}
							}
						}
						fileLock.Unlock()
					}
				}

				fileLock.Lock()
				git.writeCommitFile(filepath.Join(currentDir, "commits.txt"), commits)
				git.writeMemberFile(filepath.Join(currentDir, "members.txt"), projectData[0].Members)
				os.WriteFile(filepath.Join(currentDir, "branch_"+strings.ReplaceAll(branch.Name, " ", "_")+".txt"),
					[]byte(branch.Name), os.ModePerm)
				fileLock.Unlock()

			}

			mutex.Lock()
			report[reportIndex] = git.createDownloadInfoCSVColumn(project, branch, commits,
				projectData[0].Members, repo, downloadedFiles, currentDir)
			done++
			fmt.Printf("\rDownloaded %0*d/%d Repositories", padTo, done, all)
			mutex.Unlock()

		})

	}

	pool.StopAndWait()

	println("")

	if len(report) > 0 {
		return os.WriteFile(filepath.Join(args.OutDir, "report.csv"),
			[]byte(strings.Join(report, "\n")), os.ModePerm)
	}

	return nil
}
