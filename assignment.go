package main

import "time"

type Assignment struct {
	FindTeamRegex        []string
	FindGroupRegex       []string
	AccessLevel          string
	RepositoryName       string
	UpstreamRepositoryID int
	SubmissionBranch     string
	Deadline             *time.Time
	Subfolders           []string
	Team                 *Team
	upstream             *Repository
}

// type Assignments struct {
// 	Assignments map[string]Assignment
// }

// func NewAssignments() map[string]Assignment {
// 	// func NewAssignments() *Assignments {
// 	assignments := &Assignments{}
// 	assignments.Assignments = make(map[string]Assignment)

// 	assignments.Assignments["0"] = Assignment{RepositoryName: "a0", MemberCount: 1, UpstreamRepositoryID: -1, FindTeamRegex: []string{"1", "2"}}
// 	assignments.Assignments["1"] = Assignment{RepositoryName: "a1", MemberCount: 1, UpstreamRepositoryID: -1, FindTeamRegex: []string{"1", "2"}}

// 	// return assignments
// 	return assignments.Assignments
// }
