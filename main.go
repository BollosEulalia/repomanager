package main

import (
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/alexflint/go-arg"
)

func printErrorMessageAndExitIfError(err error) {
	//fmt.Fprintln(os.Stderr, err)
	if err != nil {
		printError(err.Error())
		os.Exit(1)
	}
}

func updateDatabase(args *CLIArgs, db *Database) {

	var err error = nil

	db.PadTeamNamesToLength()

	if args.useOfflineDatabase {
		err = db.WriteFile(args.Database)
	} else {
		err = db.UpdateDatabaseOnline()
		fmt.Printf("Pushed updated %v to repository with ID %v\n", db.configRepo.DatabasePath, db.configRepo.ConfigRepoID)
	}

	if err != nil {
		printErrorMessageAndExitIfError(err)
	}

}

func update(args *CLIArgs, db *Database, git *Gitlab, fullConfig *Config) {

	printInfo("Updating database")

	teams, err := db.CreateTeams(Args.Assignments)

	if err != nil {
		printErrorMessageAndExitIfError(err)
	}

	db.UpdateGitlabIDs()

	projects, err := git.GetProjects(teams, args.Search)
	if err != nil {
		printErrorMessageAndExitIfError(err)
	}

	git.CombineProjectsAndTeams(projects, teams)

	updateDatabase(args, db)

}

func getRepositoriesForDownload(args *CLIArgs, db *Database) []*Repository {

	allRepos := make(map[string]*Repository, len(db.Students))

	for i := range db.Students {
		groupMatches := false

		for g := range args.Groups {
			if strings.Contains(strings.ToLower(db.Students[i].Groups), strings.ToLower(args.Groups[g])) {
				groupMatches = true
				break
			}
		}

		if groupMatches {

			for repoIndex := range db.Students[i].Repos {

				assignmentMatches := false
				searchMatches := false

				if len(args.Assignments) > 0 {
					for a := range args.Assignments {
						if strings.Contains(strings.ToLower(repoIndex), strings.ToLower(args.Assignments[a])) {
							// allRepos[db.Students[i].Repos[repoIndex].ID] = db.Students[i].Repos[repoIndex]
							assignmentMatches = true
							break
						}
					}

				} else {
					assignmentMatches = true
				}
				for s := range args.Search {
					if strings.Contains(strings.ToLower(db.Students[i].Repos[repoIndex].SshURL),
						strings.ToLower(args.Search[s])) {
						searchMatches = true
						break
					}
				}

				if groupMatches && searchMatches && assignmentMatches {
					allRepos[db.Students[i].Repos[repoIndex].ID] = db.Students[i].Repos[repoIndex]
					allRepos[db.Students[i].Repos[repoIndex].ID].AddUser(db.Students[i])
				}
			}
		}
	}

	repos := make([]*Repository, 0, len(allRepos))

	for _, repo := range allRepos {
		repos = append(repos, repo)
	}

	return repos
}

func download(args *CLIArgs, db *Database, git *Gitlab) {

	repos := getRepositoriesForDownload(args, db)

	sort.SliceStable(repos, func(i int, j int) bool {
		return repos[i].SshURL < repos[j].SshURL
	})

	err := git.DownloadSubmissions(repos, args)

	if err != nil {
		printErrorMessageAndExitIfError(err)
	}

}

func createRepos(args *CLIArgs, db *Database, git *Gitlab, fullConfig *Config) {

	teams, err := db.CreateTeams(Args.Assignments)

	if err != nil {
		printErrorMessageAndExitIfError(err)
	}

	assignments := make(map[string]*Assignment, len(fullConfig.Assignments))

	for _, a := range args.Assignments {
		assignments[a] = fullConfig.Assignments[a]
	}

	if err = git.CreateRepositories(assignments, teams); err != nil {
		printErrorMessageAndExitIfError(err)
	}

	if !args.DryRun {
		updateDatabase(args, db)
	}

}

func prepare(args *CLIArgs) (*Database, *Gitlab, *Config) {

	configRepo, err := NewConfig(args.Config)
	if err != nil {
		printErrorMessageAndExitIfError(err)
	}

	// fullConfig, err := Read
	var fullConfig *Config

	if len(args.FullConfig) < 1 {
		if fullConfig, err = configRepo.GetConfigOnline(); err != nil {
			printErrorMessageAndExitIfError(err)
		}
	} else {
		if fullConfig, err = configRepo.GetConfigOffline(args.FullConfig); err != nil {
			printErrorMessageAndExitIfError(err)
		}
	}

	args.SetBranchAndDeadline(fullConfig)

	git, err := NewGitlab(fullConfig.Gitlab.APIToken, fullConfig.Gitlab.BaseURL, Args.Branch)

	if err != nil {
		printErrorMessageAndExitIfError(err)
	}
	if err = git.Init(fullConfig.Assignments, fullConfig.Gitlab); err != nil {
		printErrorMessageAndExitIfError(err)
	}

	db, err := NewDatabase(configRepo)
	if err != nil {
		printErrorMessageAndExitIfError(err)
	}

	if args.useOfflineDatabase {
		err = db.ReadDatabaseOffline(args.Database)
	} else {
		err = db.ReadDatabaseOnline()
	}

	if err != nil {
		printErrorMessageAndExitIfError(err)
	}

	return db, git, fullConfig

}

func main() {

	startTime := time.Now()

	arg.MustParse(&Args)

	if cliParseErr := parseArgs(&Args); cliParseErr != nil {
		printErrorMessageAndExitIfError(cliParseErr)
	}

	db, git, fullConfig := prepare(&Args)

	//---------------------------------------------------------------------------
	// do action based on cli args

	if len(Args.Import) > 0 {
		db.ImportData(Args.Import)
		updateDatabase(&Args, db)
	}

	if Args.Update {
		update(&Args, db, git, fullConfig)
	}

	if Args.CreateRepos {
		createRepos(&Args, db, git, fullConfig)
	}

	if Args.Download {
		download(&Args, db, git)
	}

	fmt.Printf("Operations done in %v \n", time.Since(startTime))

}
